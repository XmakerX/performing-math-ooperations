#include "math.h"

double SummOfGeometicProgress(double first, double step, int size, bool &error)
{
    if (step == 1)
    {error = true; return 0;}
    else {
        return (first*(1 - pow(step,size)))/(1-step);
    }
}
