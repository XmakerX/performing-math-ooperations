#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functions.h"
#include <QRegExp>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),m_doubleValidator(-1000, 1000, 2, this)
{

    ui->setupUi(this);
    m_doubleValidator.setNotation(QDoubleValidator::StandardNotation);
    // QRegExp RegAddNum("[0-9]{1,15}[.,]{0,1}[0-9]{0,10}");
    // QValidator num("[0-9]{1,15}[.,]{0,1}[0-9]{0,10}");
    ui->aline->setValidator(&m_doubleValidator);
    ui->bline->setValidator(&m_doubleValidator);
    ui->aquad->setValidator(&m_doubleValidator);
    ui->bquad->setValidator(&m_doubleValidator);
    ui->cquad->setValidator(&m_doubleValidator);
    ui->athird->setValidator(&m_doubleValidator);
    ui->bthird->setValidator(&m_doubleValidator);
    ui->cthird->setValidator(&m_doubleValidator);
    ui->dthird->setValidator(&m_doubleValidator);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    if(!ui->aline->text().isEmpty() and !ui->bline->text().isEmpty())
    {
        QVector<float> vec = {ui->aline->text().toFloat(),ui->bline->text().toFloat()};
        bool error = false;
        QString x1 = QString::number(LinearEquation(vec,error),'g',3);
        ui->x2->clear();
        ui->x3->clear();
        if (error == true)
            ui->x1->setText("ERROR");

        else {
            ui->x1->setText(x1);

        }
    }
    else {

    }
}

void MainWindow::on_pushButton_2_clicked()
{
    if(!ui->aquad->text().isEmpty() and !ui->bquad->text().isEmpty() and
            !ui->cquad->text().isEmpty())
    {
        QString s;
        QVector<float> vec = {ui->aquad->text().toFloat(),ui->bquad->text().toFloat(),
                              ui->cquad->text().toFloat()};
        bool error = false;
        QuadraticEquation(vec,error);
        ui->x3->clear();
        if (error == true)
        {
            ui->x1->setText("ERROR");
            ui->x2->setText("ERROR");
        }
        else {
            ui->x1->setText(s.setNum(vec[0],'g',3));
            ui->x2->setText(s.setNum(vec[1],'g',3));

        }
    }
    else {

    }
}

void MainWindow::on_pushButton_3_clicked()
{

    if(!ui->athird->text().isEmpty() and !ui->bthird->text().isEmpty()
            and !ui->cthird->text().isEmpty() and !ui->dthird->text().isEmpty())
    {

        int error = 0;
        QVector<double> values = {ui->athird->text().toDouble(),
                                  ui->bthird->text().toDouble(),
                                  ui->cthird->text().toDouble(),
                                  ui->dthird->text().toDouble()};
        CubicEquation(values,error);
        QString x1 = QString::number(values[0],'f',3);
        QString x2 = QString::number(values[1],'f',3);
        QString x3 = QString::number(values[2],'f',3);
        if (error == 1)
        {
            ui->x1->setText("ERROR");
            ui->x2->setText("ERROR");
            ui->x3->setText("ERROR");

        }
        else if (error == 2) {
            ui->x1->setText(x1);
            ui->x2->setText(x2);
            ui->x3->setText("ERROR");

        }
        else {
            ui->x1->setText(x1);
            ui->x2->setText(x2);
            ui->x3->setText(x3);
        }
    }
    else {

    }
}



void MainWindow::on_pushButton_5_clicked()
{
    ui->firstel->setValue(qrand() % ((high + 1) - low) + low);
    ui->step->setValue(qrand() % ((high/10 + 1) - low) + low);
    ui->numi->setValue(qrand() % ((high/10 + 1) - low) + low);
}

void MainWindow::on_searchi_clicked()
{
    ui->valuei->setText(QString::number(ProgressElementSearch(ui->firstel->value(),
       ui->step->value(),ui->numi->value()),'f',3));
}

void MainWindow::on_pushButton_8_clicked()
{
    ui->firstelsumma->setValue(qrand() % ((high + 1) - low) + low);
    ui->stepsumma->setValue(qrand() % ((high/10 + 1) - low) + low);
    ui->sizesumma->setValue(qrand() % ((high/10 + 1) - low) + low);
}

void MainWindow::on_searchisumm_clicked()
{
    ui->valueisumma->setText(QString::number(SummElementsProgress(ui->firstelsumma->value(),
       ui->stepsumma->value(),ui->sizesumma->value()),'f',3));
}

void MainWindow::on_randomg_clicked()
{
    ui->firstelsummg->setValue(qrand() % ((high + 1) - low) + low);
    ui->stepsummg->setValue(qrand() % ((high/10 + 1) - low) + low);
    ui->sizesummg->setValue(qrand() % ((high/10 + 1) - low) + low);
    while (ui->stepsummg->value() == 1)
    {ui->stepsummg->setValue(qrand() % ((high/10 + 1) - low) + low);}
}

void MainWindow::on_searchisummg_clicked()
{
    bool error = false;
    ui->valueisummg->setText(QString::number(SummOfGeometicProgress(ui->firstelsummg->value(),
       ui->stepsummg->value(),ui->sizesummg->value(),error),'f',3));
    if(error)
    {ui->valueisummg->setText("ERROR");}
}


