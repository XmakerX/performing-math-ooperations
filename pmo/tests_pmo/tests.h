#ifndef TESTS_H
#define TESTS_H



#include <QtCore>
#include <QtTest/QtTest>


class Tests : public QObject
{
    Q_OBJECT

public:
    Tests();

private slots:
void test_case1();
void test_case2();
void test_case3();
void test_case4();
void test_case5();
void test_case6();
void test_case7();
void test_case8();
};


#endif // TESTS_H
