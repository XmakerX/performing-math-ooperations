#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QVector>


float LinearEquation(QVector<float> &vec, bool &error);

void QuadraticEquation(QVector<float> &vec, bool &error);

void CubicEquation(QVector<double> &vec, int &error);

double ProgressElementSearch(double first, double step, int i);

double SummElementsProgress(double first, double step, int size);

double SummOfGeometicProgress(double first, double step, int size, bool &error);

double Summ(double first,double second);

double Difference(double first,double second);

#endif // FUNCTIONS_H
