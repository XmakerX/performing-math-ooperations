
double SummElementsProgress(double first, double step, int size)
{
    double an = first + step*(size-1);
    return (first + an)*size/2;

}
