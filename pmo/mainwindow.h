#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDoubleValidator>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_searchi_clicked();

    void on_pushButton_8_clicked();

    void on_searchisumm_clicked();

    void on_randomg_clicked();

    void on_searchisummg_clicked();

private:
    Ui::MainWindow *ui;
    QDoubleValidator m_doubleValidator;
    int low = 1;
    int high = 1000;
};

#endif // MAINWINDOW_H
