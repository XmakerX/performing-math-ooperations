#include <QVector>
#include "math.h"


#define M_PI (3.141592653589793)
#define M_2PI (2.*M_PI)

void CubicEquation(QVector<double> &vec, int &error) {

  if(vec[0] != 0)
  {
  double a = vec[1]/vec[0];
  double b = vec[2]/vec[0];
  double c = vec[3]/vec[0];
  vec.clear();
  double q,r,r2,q3;
  q=(a*a-3.*b)/9.; r=(a*(2.*a*a-9.*b)+27.*c)/54.;
  r2=r*r; q3=q*q*q;
  if(r2<q3) {
    double t=acos(r/sqrt(q3));
    a/=3.; q=-2.*sqrt(q);

    vec.append(QString::number(q*cos(t/3.)-a,'f',3).toDouble());
    vec.append(QString::number(q*cos((t+M_2PI)/3.)-a,'f',3).toDouble());
    vec.append(QString::number(cos((t-M_2PI)/3.)-a,'f',3).toDouble());
   // return(x);
  }
  else {
    double aa,bb;
    if(r<=0.) r=-r;
    aa=-pow(r+sqrt(r2-q3),1./3.);
    if(aa!=0.) bb=q/aa;
    else bb=0.;
    a/=3.; q=aa+bb; r=aa-bb;
    vec.append(QString::number(q-a,'f',3).toDouble());
    vec.append(QString::number((-0.5)*q-a,'f',3).toDouble());
    vec.append(QString::number((sqrt(3.)*0.5)*fabs(r),'f',3).toDouble());
    error = 2;
   // if(x[2]==0.) ;return(x);
   // return(x);
  }
  }else {
    error = 1;
}
}

