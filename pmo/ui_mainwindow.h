/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QTabWidget *tabWidget;
    QWidget *tab_3;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_15;
    QLineEdit *x1;
    QLabel *label_16;
    QLineEdit *x2;
    QLabel *label_18;
    QLineEdit *x3;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLineEdit *aline;
    QLabel *label;
    QLineEdit *bline;
    QLabel *label_2;
    QPushButton *pushButton;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_5;
    QLineEdit *aquad;
    QLabel *label_6;
    QLineEdit *bquad;
    QLabel *label_9;
    QLineEdit *cquad;
    QLabel *label_7;
    QPushButton *pushButton_2;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout_7;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_11;
    QLineEdit *athird;
    QLabel *label_17;
    QLineEdit *bthird;
    QLabel *label_12;
    QLineEdit *cthird;
    QLabel *label_13;
    QLineEdit *dthird;
    QLabel *label_14;
    QPushButton *pushButton_3;
    QWidget *tab_4;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_4;
    QDoubleSpinBox *firstel;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_8;
    QDoubleSpinBox *step;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_10;
    QDoubleSpinBox *numi;
    QPushButton *pushButton_5;
    QPushButton *searchi;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_19;
    QLineEdit *valuei;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_5;
    QHBoxLayout *horizontalLayout_20;
    QLabel *label_28;
    QDoubleSpinBox *firstelsumma;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_29;
    QDoubleSpinBox *stepsumma;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_30;
    QDoubleSpinBox *sizesumma;
    QPushButton *pushButton_8;
    QPushButton *searchisumm;
    QHBoxLayout *horizontalLayout_23;
    QLabel *label_31;
    QLineEdit *valueisumma;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_6;
    QHBoxLayout *horizontalLayout_24;
    QLabel *label_32;
    QDoubleSpinBox *firstelsummg;
    QHBoxLayout *horizontalLayout_25;
    QLabel *label_33;
    QDoubleSpinBox *stepsummg;
    QHBoxLayout *horizontalLayout_26;
    QLabel *label_34;
    QDoubleSpinBox *sizesummg;
    QPushButton *randomg;
    QPushButton *searchisummg;
    QHBoxLayout *horizontalLayout_27;
    QLabel *label_35;
    QLineEdit *valueisummg;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(742, 383);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setAutoFillBackground(false);
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        layoutWidget = new QWidget(tab_3);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 160, 283, 22));
        horizontalLayout_6 = new QHBoxLayout(layoutWidget);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        label_15 = new QLabel(layoutWidget);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        horizontalLayout_6->addWidget(label_15);

        x1 = new QLineEdit(layoutWidget);
        x1->setObjectName(QString::fromUtf8("x1"));
        x1->setMaximumSize(QSize(51, 20));
        x1->setReadOnly(true);

        horizontalLayout_6->addWidget(x1);

        label_16 = new QLabel(layoutWidget);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        horizontalLayout_6->addWidget(label_16);

        x2 = new QLineEdit(layoutWidget);
        x2->setObjectName(QString::fromUtf8("x2"));
        x2->setMaximumSize(QSize(51, 20));
        x2->setReadOnly(true);

        horizontalLayout_6->addWidget(x2);

        label_18 = new QLabel(layoutWidget);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_6->addWidget(label_18);

        x3 = new QLineEdit(layoutWidget);
        x3->setObjectName(QString::fromUtf8("x3"));
        x3->setMaximumSize(QSize(51, 20));
        x3->setReadOnly(true);

        horizontalLayout_6->addWidget(x3);

        layoutWidget1 = new QWidget(tab_3);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(11, 10, 354, 25));
        horizontalLayout = new QHBoxLayout(layoutWidget1);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMaximumSize(QSize(123, 20));

        horizontalLayout_4->addWidget(label_3);

        aline = new QLineEdit(layoutWidget1);
        aline->setObjectName(QString::fromUtf8("aline"));
        aline->setMaximumSize(QSize(31, 20));

        horizontalLayout_4->addWidget(aline);

        label = new QLabel(layoutWidget1);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMaximumSize(QSize(29, 20));
        label->setLineWidth(1);
        label->setTextFormat(Qt::AutoText);
        label->setAlignment(Qt::AlignCenter);
        label->setWordWrap(false);

        horizontalLayout_4->addWidget(label);

        bline = new QLineEdit(layoutWidget1);
        bline->setObjectName(QString::fromUtf8("bline"));
        bline->setMaximumSize(QSize(31, 20));

        horizontalLayout_4->addWidget(bline);

        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(31, 19));
        label_2->setMaximumSize(QSize(31, 19));
        label_2->setLineWidth(1);
        label_2->setTextFormat(Qt::AutoText);
        label_2->setAlignment(Qt::AlignCenter);
        label_2->setWordWrap(false);

        horizontalLayout_4->addWidget(label_2);


        horizontalLayout->addLayout(horizontalLayout_4);

        pushButton = new QPushButton(layoutWidget1);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        layoutWidget2 = new QWidget(tab_3);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(11, 40, 450, 25));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_5 = new QLabel(layoutWidget2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_2->addWidget(label_5);

        aquad = new QLineEdit(layoutWidget2);
        aquad->setObjectName(QString::fromUtf8("aquad"));
        aquad->setMaximumSize(QSize(31, 20));

        horizontalLayout_2->addWidget(aquad);

        label_6 = new QLabel(layoutWidget2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setLineWidth(1);
        label_6->setTextFormat(Qt::AutoText);
        label_6->setAlignment(Qt::AlignCenter);
        label_6->setWordWrap(false);

        horizontalLayout_2->addWidget(label_6);

        bquad = new QLineEdit(layoutWidget2);
        bquad->setObjectName(QString::fromUtf8("bquad"));
        bquad->setMaximumSize(QSize(31, 20));

        horizontalLayout_2->addWidget(bquad);

        label_9 = new QLabel(layoutWidget2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setLineWidth(1);
        label_9->setTextFormat(Qt::AutoText);
        label_9->setAlignment(Qt::AlignCenter);
        label_9->setWordWrap(false);

        horizontalLayout_2->addWidget(label_9);

        cquad = new QLineEdit(layoutWidget2);
        cquad->setObjectName(QString::fromUtf8("cquad"));
        cquad->setMaximumSize(QSize(31, 20));

        horizontalLayout_2->addWidget(cquad);

        label_7 = new QLabel(layoutWidget2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(31, 19));
        label_7->setMaximumSize(QSize(41, 19));
        label_7->setLineWidth(1);
        label_7->setTextFormat(Qt::AutoText);
        label_7->setAlignment(Qt::AlignCenter);
        label_7->setWordWrap(false);

        horizontalLayout_2->addWidget(label_7);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        pushButton_2 = new QPushButton(layoutWidget2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_3->addWidget(pushButton_2);

        layoutWidget3 = new QWidget(tab_3);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(11, 70, 525, 25));
        horizontalLayout_7 = new QHBoxLayout(layoutWidget3);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_11 = new QLabel(layoutWidget3);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_5->addWidget(label_11);

        athird = new QLineEdit(layoutWidget3);
        athird->setObjectName(QString::fromUtf8("athird"));
        athird->setMaximumSize(QSize(31, 20));

        horizontalLayout_5->addWidget(athird);

        label_17 = new QLabel(layoutWidget3);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setLineWidth(1);
        label_17->setTextFormat(Qt::AutoText);
        label_17->setAlignment(Qt::AlignCenter);
        label_17->setWordWrap(false);

        horizontalLayout_5->addWidget(label_17);

        bthird = new QLineEdit(layoutWidget3);
        bthird->setObjectName(QString::fromUtf8("bthird"));
        bthird->setMaximumSize(QSize(31, 20));

        horizontalLayout_5->addWidget(bthird);

        label_12 = new QLabel(layoutWidget3);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setLineWidth(1);
        label_12->setTextFormat(Qt::AutoText);
        label_12->setAlignment(Qt::AlignCenter);
        label_12->setWordWrap(false);

        horizontalLayout_5->addWidget(label_12);

        cthird = new QLineEdit(layoutWidget3);
        cthird->setObjectName(QString::fromUtf8("cthird"));
        cthird->setMaximumSize(QSize(31, 20));

        horizontalLayout_5->addWidget(cthird);

        label_13 = new QLabel(layoutWidget3);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setLineWidth(1);
        label_13->setTextFormat(Qt::AutoText);
        label_13->setAlignment(Qt::AlignCenter);
        label_13->setWordWrap(false);

        horizontalLayout_5->addWidget(label_13);

        dthird = new QLineEdit(layoutWidget3);
        dthird->setObjectName(QString::fromUtf8("dthird"));
        dthird->setMaximumSize(QSize(31, 20));

        horizontalLayout_5->addWidget(dthird);

        label_14 = new QLabel(layoutWidget3);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(31, 19));
        label_14->setMaximumSize(QSize(41, 19));
        label_14->setLineWidth(1);
        label_14->setTextFormat(Qt::AutoText);
        label_14->setAlignment(Qt::AlignCenter);
        label_14->setWordWrap(false);

        horizontalLayout_5->addWidget(label_14);


        horizontalLayout_7->addLayout(horizontalLayout_5);

        pushButton_3 = new QPushButton(layoutWidget3);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout_7->addWidget(pushButton_3);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        groupBox = new QGroupBox(tab_4);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(0, 10, 508, 84));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_8->addWidget(label_4);

        firstel = new QDoubleSpinBox(groupBox);
        firstel->setObjectName(QString::fromUtf8("firstel"));
        firstel->setDecimals(3);
        firstel->setMinimum(-9999990.000000000000000);
        firstel->setMaximum(9999999.990000000223517);
        firstel->setSingleStep(0.050000000000000);

        horizontalLayout_8->addWidget(firstel);


        gridLayout->addLayout(horizontalLayout_8, 0, 0, 1, 2);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_9->addWidget(label_8);

        step = new QDoubleSpinBox(groupBox);
        step->setObjectName(QString::fromUtf8("step"));
        step->setDecimals(3);
        step->setMinimum(-9990.000000000000000);
        step->setMaximum(9999.989999999999782);
        step->setSingleStep(0.050000000000000);

        horizontalLayout_9->addWidget(step);


        gridLayout->addLayout(horizontalLayout_9, 0, 2, 1, 1);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_10->addWidget(label_10);

        numi = new QDoubleSpinBox(groupBox);
        numi->setObjectName(QString::fromUtf8("numi"));
        numi->setDecimals(0);
        numi->setMinimum(1.000000000000000);
        numi->setMaximum(10000.000000000000000);
        numi->setSingleStep(1.000000000000000);

        horizontalLayout_10->addWidget(numi);


        gridLayout->addLayout(horizontalLayout_10, 0, 3, 1, 1);

        pushButton_5 = new QPushButton(groupBox);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        gridLayout->addWidget(pushButton_5, 1, 0, 1, 1);

        searchi = new QPushButton(groupBox);
        searchi->setObjectName(QString::fromUtf8("searchi"));

        gridLayout->addWidget(searchi, 1, 1, 1, 1);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        label_19 = new QLabel(groupBox);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        horizontalLayout_11->addWidget(label_19);

        valuei = new QLineEdit(groupBox);
        valuei->setObjectName(QString::fromUtf8("valuei"));
        valuei->setReadOnly(true);

        horizontalLayout_11->addWidget(valuei);


        gridLayout->addLayout(horizontalLayout_11, 1, 2, 1, 2);

        groupBox_3 = new QGroupBox(tab_4);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(0, 100, 508, 84));
        gridLayout_5 = new QGridLayout(groupBox_3);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        label_28 = new QLabel(groupBox_3);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        horizontalLayout_20->addWidget(label_28);

        firstelsumma = new QDoubleSpinBox(groupBox_3);
        firstelsumma->setObjectName(QString::fromUtf8("firstelsumma"));
        firstelsumma->setDecimals(3);
        firstelsumma->setMinimum(-9999990.000000000000000);
        firstelsumma->setMaximum(9999999.990000000223517);
        firstelsumma->setSingleStep(0.050000000000000);

        horizontalLayout_20->addWidget(firstelsumma);


        gridLayout_5->addLayout(horizontalLayout_20, 0, 0, 1, 2);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        label_29 = new QLabel(groupBox_3);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        horizontalLayout_21->addWidget(label_29);

        stepsumma = new QDoubleSpinBox(groupBox_3);
        stepsumma->setObjectName(QString::fromUtf8("stepsumma"));
        stepsumma->setDecimals(3);
        stepsumma->setMinimum(-9990.000000000000000);
        stepsumma->setMaximum(9999.989999999999782);
        stepsumma->setSingleStep(0.050000000000000);

        horizontalLayout_21->addWidget(stepsumma);


        gridLayout_5->addLayout(horizontalLayout_21, 0, 2, 1, 1);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QString::fromUtf8("horizontalLayout_22"));
        label_30 = new QLabel(groupBox_3);
        label_30->setObjectName(QString::fromUtf8("label_30"));

        horizontalLayout_22->addWidget(label_30);

        sizesumma = new QDoubleSpinBox(groupBox_3);
        sizesumma->setObjectName(QString::fromUtf8("sizesumma"));
        sizesumma->setDecimals(0);
        sizesumma->setMinimum(1.000000000000000);
        sizesumma->setMaximum(10000.000000000000000);
        sizesumma->setSingleStep(1.000000000000000);

        horizontalLayout_22->addWidget(sizesumma);


        gridLayout_5->addLayout(horizontalLayout_22, 0, 3, 1, 1);

        pushButton_8 = new QPushButton(groupBox_3);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));

        gridLayout_5->addWidget(pushButton_8, 1, 0, 1, 1);

        searchisumm = new QPushButton(groupBox_3);
        searchisumm->setObjectName(QString::fromUtf8("searchisumm"));

        gridLayout_5->addWidget(searchisumm, 1, 1, 1, 1);

        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setObjectName(QString::fromUtf8("horizontalLayout_23"));
        label_31 = new QLabel(groupBox_3);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        horizontalLayout_23->addWidget(label_31);

        valueisumma = new QLineEdit(groupBox_3);
        valueisumma->setObjectName(QString::fromUtf8("valueisumma"));
        valueisumma->setReadOnly(true);

        horizontalLayout_23->addWidget(valueisumma);


        gridLayout_5->addLayout(horizontalLayout_23, 1, 2, 1, 2);

        groupBox_4 = new QGroupBox(tab_4);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(0, 190, 508, 84));
        gridLayout_6 = new QGridLayout(groupBox_4);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QString::fromUtf8("horizontalLayout_24"));
        label_32 = new QLabel(groupBox_4);
        label_32->setObjectName(QString::fromUtf8("label_32"));

        horizontalLayout_24->addWidget(label_32);

        firstelsummg = new QDoubleSpinBox(groupBox_4);
        firstelsummg->setObjectName(QString::fromUtf8("firstelsummg"));
        firstelsummg->setDecimals(3);
        firstelsummg->setMinimum(-9999990.000000000000000);
        firstelsummg->setMaximum(9999999.990000000223517);
        firstelsummg->setSingleStep(0.050000000000000);

        horizontalLayout_24->addWidget(firstelsummg);


        gridLayout_6->addLayout(horizontalLayout_24, 0, 0, 1, 2);

        horizontalLayout_25 = new QHBoxLayout();
        horizontalLayout_25->setSpacing(6);
        horizontalLayout_25->setObjectName(QString::fromUtf8("horizontalLayout_25"));
        label_33 = new QLabel(groupBox_4);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        horizontalLayout_25->addWidget(label_33);

        stepsummg = new QDoubleSpinBox(groupBox_4);
        stepsummg->setObjectName(QString::fromUtf8("stepsummg"));
        stepsummg->setDecimals(3);
        stepsummg->setMinimum(-9990.000000000000000);
        stepsummg->setMaximum(9999.989999999999782);
        stepsummg->setSingleStep(0.050000000000000);

        horizontalLayout_25->addWidget(stepsummg);


        gridLayout_6->addLayout(horizontalLayout_25, 0, 2, 1, 1);

        horizontalLayout_26 = new QHBoxLayout();
        horizontalLayout_26->setSpacing(6);
        horizontalLayout_26->setObjectName(QString::fromUtf8("horizontalLayout_26"));
        label_34 = new QLabel(groupBox_4);
        label_34->setObjectName(QString::fromUtf8("label_34"));

        horizontalLayout_26->addWidget(label_34);

        sizesummg = new QDoubleSpinBox(groupBox_4);
        sizesummg->setObjectName(QString::fromUtf8("sizesummg"));
        sizesummg->setDecimals(0);
        sizesummg->setMinimum(1.000000000000000);
        sizesummg->setMaximum(10000.000000000000000);
        sizesummg->setSingleStep(1.000000000000000);

        horizontalLayout_26->addWidget(sizesummg);


        gridLayout_6->addLayout(horizontalLayout_26, 0, 3, 1, 1);

        randomg = new QPushButton(groupBox_4);
        randomg->setObjectName(QString::fromUtf8("randomg"));

        gridLayout_6->addWidget(randomg, 1, 0, 1, 1);

        searchisummg = new QPushButton(groupBox_4);
        searchisummg->setObjectName(QString::fromUtf8("searchisummg"));

        gridLayout_6->addWidget(searchisummg, 1, 1, 1, 1);

        horizontalLayout_27 = new QHBoxLayout();
        horizontalLayout_27->setSpacing(6);
        horizontalLayout_27->setObjectName(QString::fromUtf8("horizontalLayout_27"));
        label_35 = new QLabel(groupBox_4);
        label_35->setObjectName(QString::fromUtf8("label_35"));

        horizontalLayout_27->addWidget(label_35);

        valueisummg = new QLineEdit(groupBox_4);
        valueisummg->setObjectName(QString::fromUtf8("valueisummg"));
        valueisummg->setReadOnly(true);

        horizontalLayout_27->addWidget(valueisummg);


        gridLayout_6->addLayout(horizontalLayout_27, 1, 2, 1, 2);

        tabWidget->addTab(tab_4, QString());

        gridLayout_3->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 742, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600;\">X</span><span style=\" font-size:12pt; font-weight:600; vertical-align:sub;\">1 </span><span style=\" font-size:12pt; font-weight:600;\">=</span></p></body></html>", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">X</span><span style=\" font-size:12pt; font-weight:600; font-style:italic; vertical-align:sub;\">2</span><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">=</span></p></body></html>", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">X</span><span style=\" font-size:12pt; font-weight:600; font-style:italic; vertical-align:sub;\">3</span><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">=</span></p></body></html>", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">\320\233\320\270\320\275\320\265\320\271\320\275\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265:</span></p></body></html>", nullptr));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">X +</span></p></body></html>", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">= 0</span></p></body></html>", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">\320\232\320\262\320\260\320\264\321\200\320\260\321\202\320\275\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265:</span></p></body></html>", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">X </span><span style=\" font-size:12pt; font-weight:600; font-style:italic; vertical-align:super;\">2</span><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">+</span></p></body></html>", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">X +</span></p></body></html>", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">= 0</span></p></body></html>", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">\320\232\321\203\320\261\320\270\321\207\320\265\321\201\320\272\320\276\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\265:</span></p></body></html>", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">X </span><span style=\" font-size:12pt; font-weight:600; font-style:italic; vertical-align:super;\">3</span><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">+</span></p></body></html>", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">X </span><span style=\" font-size:12pt; font-weight:600; font-style:italic; vertical-align:super;\">2</span><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">+</span></p></body></html>", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">X +</span></p></body></html>", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; font-weight:600; font-style:italic;\">= 0</span></p></body></html>", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\265\320\275\320\270\320\265 \321\203\321\200\320\260\320\262\320\275\320\265\320\275\320\270\320\271", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "\320\235\320\260\321\205\320\276\320\266\320\264\320\265\320\275\320\270\320\265 i-\320\263\320\276 \321\207\320\273\320\265\320\275\320\260 \320\260\321\200\320\270\321\204\320\274\320\265\321\202\320\270\321\207\320\265\321\201\320\272\320\276\320\271 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\237\320\265\321\200\320\262\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202:</span></p></body></html>", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\250\320\260\320\263:</span></p></body></html>", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\235\320\276\320\274\320\265\321\200 \321\215\320\273\320\265\320\274\320\265\320\275\321\202\320\260:</span></p></body></html>", nullptr));
        pushButton_5->setText(QApplication::translate("MainWindow", "\320\241\320\273\321\203\321\207\320\260\320\271\320\275\321\213\320\265 \321\207\320\270\321\201\320\273\320\260", nullptr));
        searchi->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\227\320\275\320\260\321\207\320\265\320\275\320\270\320\265:</span></p></body></html>", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "\320\235\320\260\321\205\320\276\320\266\320\264\320\265\320\275\320\270\320\265 \321\201\321\203\320\274\320\274\321\213 \320\260\321\200\320\270\321\204\320\274\320\265\321\202\320\270\321\207\320\265\321\201\320\272\320\276\320\271 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270", nullptr));
        label_28->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\237\320\265\321\200\320\262\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202:</span></p></body></html>", nullptr));
        label_29->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\250\320\260\320\263:</span></p></body></html>", nullptr));
        label_30->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\240\320\260\320\267\320\274\320\265\321\200\320\275\320\276\321\201\321\202\321\214:</span></p></body></html>", nullptr));
        pushButton_8->setText(QApplication::translate("MainWindow", "\320\241\320\273\321\203\321\207\320\260\320\271\320\275\321\213\320\265 \321\207\320\270\321\201\320\273\320\260", nullptr));
        searchisumm->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214", nullptr));
        label_31->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\227\320\275\320\260\321\207\320\265\320\275\320\270\320\265:</span></p></body></html>", nullptr));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "\320\235\320\260\321\205\320\276\320\266\320\264\320\265\320\275\320\270\320\265 \321\201\321\203\320\274\320\274\321\213 \320\263\320\265\320\276\320\274\320\265\321\202\321\200\320\270\321\207\320\265\321\201\320\272\320\276\320\271 \320\277\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270", nullptr));
        label_32->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\237\320\265\321\200\320\262\321\213\320\271 \321\215\320\273\320\265\320\274\320\265\320\275\321\202:</span></p></body></html>", nullptr));
        label_33->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\250\320\260\320\263:</span></p></body></html>", nullptr));
        label_34->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\240\320\260\320\267\320\274\320\265\321\200\320\275\320\276\321\201\321\202\321\214:</span></p></body></html>", nullptr));
        randomg->setText(QApplication::translate("MainWindow", "\320\241\320\273\321\203\321\207\320\260\320\271\320\275\321\213\320\265 \321\207\320\270\321\201\320\273\320\260", nullptr));
        searchisummg->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214", nullptr));
        label_35->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600; font-style:italic;\">\320\227\320\275\320\260\321\207\320\265\320\275\320\270\320\265:</span></p></body></html>", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("MainWindow", "\320\237\321\200\320\276\320\263\321\200\320\265\321\201\321\201\320\270\320\270", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
